/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;


import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.persistence.*;


@ManagedBean(name = "cerveja")
@Entity
public class Cerveja {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String nome;
    @ManyToOne(targetEntity = Tipo.class)
    private Tipo tipo;
    @ManyToOne(targetEntity = Fabricante.class)
    private Fabricante fabricante;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Ingrediente> ingredientes;   
    @ManyToOne(targetEntity = Usuario.class)
    private Usuario usuario;

    public Cerveja()
    {
    	
    }
    
    public Cerveja(int id, String nome, Tipo tipo, Fabricante fabricante, List<Ingrediente> ingredientes, Usuario usuario) {
        if (id >0)
            this.id = id;
        
        this.nome = nome;
        this.tipo = tipo;
        this.fabricante = fabricante;
        this.ingredientes = ingredientes;
        this.usuario = usuario;
    }

    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the tipo
     */
    public Tipo getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the fabricante
     */
    public Fabricante getFabricante() {
        return fabricante;
    }

    /**
     * @param fabricante the fabricante to set
     */
    public void setFabricante(Fabricante fabricante) {
        this.fabricante = fabricante;
    }

    /**
     * @return the ingredientes
     */
    public List<Ingrediente> getIngredientes() {
        return ingredientes;
    }

    /**
     * @param ingredientes the ingredientes to set
     */
    public void setIngredientes(List<Ingrediente> ingredientes) {
        this.ingredientes = ingredientes;
    }
    
    public void ingredientesClear(){
        this.ingredientes.clear();
    }
    
    public void setUsuario(Usuario usuario){
    	this.usuario = usuario;
    }
}