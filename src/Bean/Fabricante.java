/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import javax.faces.bean.ManagedBean;
import javax.persistence.*;
import DAO.DAOFabricante;

@ManagedBean(name = "fabricante")
@Entity
public class Fabricante {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String nome;
    private int cnpj;
    @ManyToOne(targetEntity = Usuario.class)
    private Usuario usuario;

    public Fabricante(int id, String nome, int cnpj) {
        this.id = id;
        this.nome = nome;
        this.cnpj = cnpj;
    }
   
    public Fabricante() {
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the cnpj
     */
    public int getCnpj() {
        return cnpj;
    }

    /**
     * @param cnpj the cnpj to set
     */
    public void setCnpj(int cnpj) {
        this.cnpj = cnpj;
    }
    
    public void setUsuario(Usuario usuario){
    	this.usuario = usuario;
    }
}