/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.persistence.*;
import DAO.DAOFabricante;

@ManagedBean(name = "ingrediente")
@Entity
public class Ingrediente {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @Transient
    private boolean checkado;
    
    private String nome;
    @ManyToOne(targetEntity = Fabricante.class)
    private Fabricante fabricante;
    
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "ingredientes", targetEntity = Cerveja.class)
    private List<Cerveja> cervejas;
    
    @ManyToOne(targetEntity = Usuario.class)
    private Usuario usuario;
    
    public Ingrediente(int id, String nome, Fabricante fabricante) {
        this.id = id;
        this.nome = nome;        
        this.fabricante = fabricante;        
    }
    
    public Ingrediente() {
        
    }    

    public List<Cerveja> getCervejas() {
        return cervejas;
    }

    public void setCervejas(List<Cerveja> cervejas) {
        this.cervejas = cervejas;
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the fabricante
     */
    public Fabricante getFabricante() {
        return fabricante;
    }

    /**
     * @param fabricante the fabricante to set
     */
    public void setFabricante(Fabricante fabricante) {
        this.fabricante = fabricante;
    }
    
    public void cervejasClear(){
        this.cervejas.clear();
    }
    public void setUsuario(Usuario usuario){
    	this.usuario = usuario;
    }
}
