/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import javax.faces.bean.ManagedBean;
import javax.persistence.*;

import DAO.DAOTipo;

@ManagedBean(name = "tipo")
@Entity
public class Tipo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String nome;

    @ManyToOne(targetEntity = Usuario.class)
    private Usuario usuario;
    
    public Tipo(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Tipo() {
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void setUsuario(Usuario usuario){
    	this.usuario = usuario;
    }
}
