/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Bean.Fabricante;
import Bean.Usuario;
import FabricaDeSessoes.FabricaDeSessoes;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;


@ManagedBean(name = "daoFabricante", eager = true)
@SessionScoped
public class DAOFabricante {
	
	@ManagedProperty(value = "#{fabricante}")
	Fabricante fabricante;
	Usuario usuario;

	public Fabricante getFabricante() {
		return fabricante;
	}

	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
	
	public void setUsuario(Usuario usuario){
		this.usuario = usuario;
	}

    static SessionFactory fabrica = FabricaDeSessoes.getSessionFactory();

    public String adicionarFabricante(Usuario usuario) {
    	this.fabricante.setUsuario(usuario);
        Session session = fabrica.openSession();
        session.beginTransaction();
        session.save(this.fabricante);
        session.getTransaction().commit();
        session.close();
        return "listarFabricante.xhtml";
    }

    public Fabricante buscaFabricante(String nome) {
        Session session = fabrica.openSession();
        Query q = session.createQuery("from Fabricante where nome = :nome");
        return (Fabricante) q.setString("nome", nome).uniqueResult();
    }

    public void atualizarFabricante(Fabricante fabricante) {
        Session session = fabrica.openSession();
        session.beginTransaction();
        session.update(fabricante);
        session.getTransaction().commit();
        session.close();
    }

    public String excluirFabricante(Fabricante fabricante) {
        Session session = fabrica.openSession();
        session.beginTransaction();
        session.delete(fabricante);
        session.getTransaction().commit();
        session.close();
        return "listarFabricante.xhtml";
    }

    public List<Fabricante> listarFabricantes(Usuario usuario) {
        Session session = fabrica.openSession();
        Query q = session.createQuery("from Fabricante where usuario_id = :usuario");
        List lista = q.setInteger("usuario", usuario.getId()).list();
        return lista;
    }

    public Fabricante retornarFabricante(int id) {
        Session session = fabrica.openSession();
        Query q = session.createQuery("from Fabricante where id = :id");
        return (Fabricante) q.setInteger("id", id).uniqueResult();
    }
}
