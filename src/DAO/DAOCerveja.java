/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Bean.Cerveja;
import Bean.Ingrediente;
import Bean.Usuario;

import static DAO.DAOIngrediente.fabrica;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "daoCerveja", eager = true)
@SessionScoped
public class DAOCerveja {

	@ManagedProperty(value = "#{cerveja}")
	Cerveja cerveja;
	Usuario usuario;
	int tipoSelecionado;
	int fabricanteSelecionado;
	List<String> ingredientesSelecionados;

	public Cerveja getCerveja() {
		return cerveja;
	}

	public void setCerveja(Cerveja cerveja) {
		this.cerveja = cerveja;
	}

	public int getTipoSelecionado() {
		return tipoSelecionado;
	}

	public void setTipoSelecionado(int tipoSelecionado) {
		this.tipoSelecionado = tipoSelecionado;
		if (cerveja != null) {
			this.cerveja.setTipo(new DAOTipo().retornarTipo(this.tipoSelecionado));
		}
	}
	
	public void setUsuario(Usuario usuario){
		this.usuario = usuario;
	}

	public int getFabricanteSelecionado() {
		return fabricanteSelecionado;
	}

	public void setFabricanteSelecionado(int fabricanteSelecionado) {
		this.fabricanteSelecionado = fabricanteSelecionado;
		if (cerveja != null) {
			this.cerveja.setFabricante(new DAOFabricante().retornarFabricante(this.fabricanteSelecionado));
		}
	}

	public List<String> getIngredientesSelecionados() {
		return ingredientesSelecionados;
	}

	public void setIngredientesSelecionados(List<String> ingredientesSelecionados) {
		this.ingredientesSelecionados = ingredientesSelecionados;
		if (cerveja != null) {
			DAOIngrediente iDAO = new DAOIngrediente();
			List<Ingrediente> ingredientes = new ArrayList<Ingrediente>();
			for (String i : ingredientesSelecionados) {
				ingredientes.add(iDAO.retornarIngrediente(Integer.parseInt(i)));
			}
			this.cerveja.setIngredientes(ingredientes);
		}
	}

	public String adicionarCerveja(Usuario usuario) {
		this.cerveja.setUsuario(usuario);
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.save(this.cerveja);
		session.getTransaction().commit();
		session.close();
		return "listarCerveja.xhtml";
	}

	public Cerveja buscaCerveja(String nome) {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from Cerveja where nome= :nome");

		Cerveja cerveja = (Cerveja) q.setString("nome", nome).uniqueResult();
		session.close();
		return cerveja;
	}

	public void atualizarCerveja(Cerveja cerveja) {
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.update(cerveja);
		session.getTransaction().commit();
		session.close();
	}

	public void excluirCerveja(Cerveja cerveja) {
		cerveja.ingredientesClear();
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.delete(cerveja);
		session.getTransaction().commit();
		session.close();
	}

	public List<Cerveja> listarCerveja(Usuario usuario) {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from Cerveja where usuario_id = :usuario");
		List lista = q.setInteger("usuario", usuario.getId()).list();
		session.close();
		return lista;
	}

	public Cerveja retornarCerveja(int id) {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from Cerveja where id = :id");
		Cerveja cerveja = (Cerveja) q.setInteger("id", id).uniqueResult();
		session.close();
		return cerveja;
	}
}
