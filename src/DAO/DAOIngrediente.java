/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Bean.Fabricante;
import Bean.Ingrediente;
import Bean.Usuario;
import FabricaDeSessoes.FabricaDeSessoes;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "daoIngrediente", eager = true)
@SessionScoped
public class DAOIngrediente {
	@ManagedProperty(value = "#{ingrediente}")
	Ingrediente ingrediente;
	Usuario usuario;
	int fabricanteSelecionado;

	public Ingrediente getIngrediente() {
		return ingrediente;
	}

	public void setIngrediente(Ingrediente ingrediente) {
		this.ingrediente = ingrediente;
	}

	public int getFabricanteSelecionado() {
		return fabricanteSelecionado;
	}

	public void setUsuario(Usuario usuario){
		this.usuario = usuario;
	}
	
	public void setFabricanteSelecionado(int fabricanteSelecionado) {
		this.fabricanteSelecionado = fabricanteSelecionado;
		if (ingrediente != null) {
			this.ingrediente.setFabricante(new DAOFabricante().retornarFabricante(this.fabricanteSelecionado));
		}
	}

	static SessionFactory fabrica = FabricaDeSessoes.getSessionFactory();

	public String adicionarIngrediente(Usuario usuario) {
		this.ingrediente.setUsuario(usuario);
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.save(this.ingrediente);
		session.getTransaction().commit();
		session.close();
		return "listarIngrediente.xhtml";
	}

	public Ingrediente buscaIngrediente(String nome) {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from Ingrediente where nome= :nome");
		return (Ingrediente) q.setString("nome", nome).uniqueResult();
	}

	public void atualizarIngrediente(Ingrediente ingrediente) {
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.update(ingrediente);
		session.getTransaction().commit();
		session.close();
	}

	public String excluirIngrediente(Ingrediente ingrediente) {
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.delete(ingrediente);
		session.getTransaction().commit();
		session.close();
		return "listarIngrediente.xhtml";
	}

	public List<Ingrediente> listarIngrediente(Usuario usuario) {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from Ingrediente where usuario_id = :usuario");
		List lista = q.setInteger("usuario", usuario.getId()).list();
		return lista;
	}

	public Ingrediente retornarIngrediente(int id) {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from Ingrediente where id = :id");
		return (Ingrediente) q.setInteger("id", id).uniqueResult();
	}
}
