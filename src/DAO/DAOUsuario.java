/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Bean.Usuario;
import Bean.Usuario;
import FabricaDeSessoes.FabricaDeSessoes;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

@ManagedBean(name = "daoUsuario", eager = true)
@SessionScoped
public class DAOUsuario {
	@ManagedProperty(value = "#{usuario}")
	Usuario usuario;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	static SessionFactory fabrica = FabricaDeSessoes.getSessionFactory();

	public String adicionarUsuario() {
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.save(this.usuario);
		session.getTransaction().commit();
		session.close();
		return "listarUsuario.xhtml";
	}

	public Usuario buscaUsuario(String nome) {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from usuario where nome= :nome");
		return (Usuario) q.setString("nome", nome).uniqueResult();
	}

	public void atualizarUsuario(Usuario usuario) {
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.update(usuario);
		session.getTransaction().commit();
		session.close();
	}

	public String excluirUsuario(Usuario usuario) {
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.delete(usuario);
		session.getTransaction().commit();
		session.close();
		return "listarUsuario.xhtml";
	}

	public List<Usuario> listarUsuario() {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from usuario");
		List lista = q.list();
		return lista;
	}

	public Usuario retornarUsuario(int id) {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from usuario where id = :id");
		return (Usuario) q.setInteger("id", id).uniqueResult();
	}

	public boolean validar() {
		Usuario usuarioAux;

		Session session = fabrica.openSession();
		Query q = session.createQuery("from usuario where nome = :nome and senha = :senha");

		q.setString("nome", usuario.getNome());
		q.setString("senha", usuario.getSenha());

		usuarioAux = (Usuario) q.uniqueResult();
		session.close();

		if (usuarioAux != null){
			usuario.setId(usuarioAux.getId());
		}
		return (usuarioAux != null);
	}
	public boolean validarNomeUsuario(){
		Usuario usuarioAux;
		
		Session session = fabrica.openSession();
		Query q = session.createQuery("from usuario where nome = :nome");

		q.setString("nome", usuario.getNome());

		usuarioAux = (Usuario) q.uniqueResult();
		
		session.close();

		if (usuarioAux != null){
			usuario.setId(usuarioAux.getId());
		}
		return (usuarioAux != null);
	}

}
