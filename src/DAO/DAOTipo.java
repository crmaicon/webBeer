/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Bean.Tipo;
import Bean.Usuario;
import FabricaDeSessoes.FabricaDeSessoes;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


@ManagedBean(name = "daoTipo", eager = true)
@SessionScoped
public class DAOTipo {
	@ManagedProperty(value = "#{tipo}")	
	Tipo tipo;
	
	Usuario usuario;
	
	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public void setUsuario(Usuario usuario){
		this.usuario = usuario;
	}
	
	static SessionFactory fabrica = FabricaDeSessoes.getSessionFactory();

	public String adicionarTipo(Usuario usuario) {
		this.tipo.setUsuario(usuario);
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.save(this.tipo);
		session.getTransaction().commit();
		session.close();
		return "listarTipo.xhtml";
	}

	public Tipo buscaTipo(String nome) {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from Tipo where nome= :nome");
		return (Tipo) q.setString("nome", nome).uniqueResult();
	}

	public void atualizarTipo(Tipo tipo) {
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.update(tipo);
		session.getTransaction().commit();
		session.close();
	}

	public String excluirTipo(Tipo tipo) {
		Session session = fabrica.openSession();
		session.beginTransaction();
		session.delete(tipo);
		session.getTransaction().commit();
		session.close();
		return "listarTipo.xhtml";
	}

	public List<Tipo> listarTipo(Usuario usuario) {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from Tipo where usuario_id = :usuario");
		List lista = q.setInteger("usuario", usuario.getId()).list();
		return lista;
	}

	public Tipo retornarTipo(int id) {
		Session session = fabrica.openSession();
		Query q = session.createQuery("from Tipo where id = :id");
		return (Tipo) q.setInteger("id", id).uniqueResult();
	}
}
